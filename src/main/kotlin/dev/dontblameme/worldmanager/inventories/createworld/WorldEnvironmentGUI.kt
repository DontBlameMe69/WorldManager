package dev.dontblameme.worldmanager.inventories.createworld

import dev.dontblameme.kutilsapi.inventory.InventoryBuilder
import dev.dontblameme.kutilsapi.itembuilder.ItemBuilder
import dev.dontblameme.worldmanager.main.WorldManager
import dev.dontblameme.worldmanager.utils.ItemGenerator
import dev.dontblameme.worldmanager.utils.WorldUtils
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.entity.Player
import java.util.*

/**
 * Creator: DontBlameMe69
 * Date: Mar 25, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class WorldEnvironmentGUI {
    private val customConfig = WorldManager.customConfig!!
    private val nether = ItemGenerator.generateItem("world_creation", "environment_selection", "nether")
    private val overworld = ItemGenerator.generateItem("world_creation", "environment_selection", "overworld")
    private val end = ItemGenerator.generateItem("world_creation", "environment_selection", "end")
    private val placeholder = ItemBuilder(
        amount = 1,
        displayName = "<red>",
        material = Material.getMaterial(customConfig.getValue("material", "world_creation", "environment_selection", "placeholder")!!)!!
    )

    fun open(player: Player, worldType: WorldUtils.CustomWorldType?) {
        val inventory = InventoryBuilder(
            customConfig.getValue("name", "world_creation", "environment_selection")!!,
            customConfig.getValue("size", "world_creation", "environment_selection")!!.toInt())

        if(player.hasPermission(customConfig.getValue("permission", "world_creation", "environment_selection", "nether")!!))
            inventory.addItem(nether, customConfig.getValue("slot", "world_creation", "environment_selection", "nether")!!.toInt()) {
                WorldChatManager(player, worldType, World.Environment.NETHER)
                player.closeInventory()
            }

        if(player.hasPermission(customConfig.getValue("permission", "world_creation", "environment_selection", "overworld")!!))
            inventory.addItem(overworld, customConfig.getValue("slot", "world_creation", "environment_selection", "overworld")!!.toInt()) {
                WorldChatManager(player, worldType, World.Environment.NORMAL)
                player.closeInventory()
            }

        if(player.hasPermission(customConfig.getValue("permission", "world_creation", "environment_selection", "end")!!))
            inventory.addItem(end, customConfig.getValue("slot", "world_creation", "environment_selection", "end")!!.toInt()) {
                WorldChatManager(player, worldType, World.Environment.THE_END)
                player.closeInventory()
            }

        if(customConfig.getValue("enabled", "global_items", "gui_back")!!.toBoolean())
            inventory.addItem(
                ItemBuilder(
                    material = Material.getMaterial(customConfig.getValue("material", "global_items", "gui_back")!!)!!,
                    displayName = customConfig.getValue("name", "global_items", "gui_back")!!,
                    amount = customConfig.getValue("amount", "global_items", "gui_back")!!.toInt(),
                    lores = LinkedList(customConfig.getList("lore", "global_items", "gui_back")!! as ArrayList<String>)
                ).build(),
                inventory.getSize() -1
            ) {
                WorldTypeGUI().open(player)
            }

        inventory.placeholder(placeholder.build())

        player.openInventory(inventory.build())
    }
}