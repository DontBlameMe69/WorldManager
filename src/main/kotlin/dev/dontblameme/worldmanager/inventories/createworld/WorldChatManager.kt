package dev.dontblameme.worldmanager.inventories.createworld

import dev.dontblameme.kutilsapi.chat.ChatInput
import dev.dontblameme.kutilsapi.textparser.TextParser.Companion.parseColors
import dev.dontblameme.worldmanager.main.WorldManager
import dev.dontblameme.worldmanager.utils.WorldUtils
import net.kyori.adventure.text.TextComponent
import org.bukkit.Bukkit
import org.bukkit.World
import org.bukkit.entity.Player
import kotlin.system.measureTimeMillis

/**
 * Creator: DontBlameMe69
 * Date: Mar 26, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

data class WorldChatManager(val player: Player, val worldType: WorldUtils.CustomWorldType?, val environment: World.Environment) {

    private var worldName = ""
    private var seed: Long? = 0
    private val config = WorldManager.customConfig!!

    init {
        awaitWorldName()
    }

    private fun awaitWorldName() {
        player.sendMessage("${config.getValue("prefix")} ${config.getValue("message", "world_creation", "world_name_selection")}".parseColors())

        ChatInput.getChatInput(player) {
            val text = it.message() as TextComponent

            if(text.content().contains("/") || text.content().contains("\\")) {
                player.sendMessage("${config.getValue("prefix")} ${config.getValue("invalid_symbol_message", "world_creation", "world_name_selection")}".parseColors())

                awaitWorldName()
                return@getChatInput
            }

            worldName = text.content()
            awaitSeed()
        }
    }

    private fun awaitSeed() {
        player.sendMessage("${config.getValue("prefix")} ${config.getValue("message", "world_creation", "seed_selection")}".parseColors())

        ChatInput.getChatInput(player) {
            val text = it.message() as TextComponent

            seed = text.content().toLongOrNull()

            createWorld()
        }
    }

    private fun createWorld() {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("WorldManager")!!) {
            var world: World?
            val timeMillis = measureTimeMillis {
                world = WorldUtils.generateWorld(
                    name = worldName,
                    environment = environment,
                    seed = seed,
                    type = worldType
                )
            }

            player.teleport(world!!.spawnLocation)
            player.sendMessage(
                "${config.getValue("prefix")} ${
                    config.getValue(
                        "success_message",
                        "world_creation"
                    )
                }".replace("%worldName", worldName).replace("%timeMillis", "$timeMillis").parseColors()
            )
        }
    }
}