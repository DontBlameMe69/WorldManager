package dev.dontblameme.worldmanager.utils.generators

import org.bukkit.generator.ChunkGenerator

/**
 * Creator: DontBlameMe69
 * Date: Mar 24, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class VoidWorld: ChunkGenerator() {

    override fun shouldGenerateCaves(): Boolean = false
    override fun shouldGenerateNoise(): Boolean = false
    override fun shouldGenerateStructures(): Boolean = false
    override fun shouldGenerateDecorations(): Boolean = false
    override fun shouldGenerateSurface(): Boolean = false

}