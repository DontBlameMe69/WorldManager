package dev.dontblameme.worldmanager.main

import dev.dontblameme.kutilsapi.config.custom.CustomConfig
import dev.dontblameme.kutilsapi.updates.UpdateManager
import dev.dontblameme.worldmanager.commands.WorldManagerCommand
import org.bukkit.plugin.java.JavaPlugin

/**
 * Creator: DontBlameMe69
 * Date: Mar 24, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class WorldManager: JavaPlugin() {

    private val updateUrl = "https://codeberg.org/DontBlameMe69/WorldManager/releases/latest"

    companion object {
        var customConfig: CustomConfig? = null
    }

    override fun onEnable() {
        // Check updates
        if(UpdateManager(updateUrl, this).checkNewVersion())
            logger.warning("There is a new version available! Download it at $updateUrl")

        customConfig = CustomConfig(this, "config.yml")

        // Commands
        WorldManagerCommand().register()

        logger.info("Successfully loaded.")
    }

    override fun onDisable() {
        logger.info("Successfully unloaded.")
    }
}